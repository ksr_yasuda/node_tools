// vim: ft=javascript noet ts=4 sts=4 sw=4

module.exports.defaultCallback		= defaultCallback;
function defaultCallback(err){
	if(err != null){
		console.error(err);
	}
}

//------------------------------------------------------------------------------

module.exports.defaultCallbackThrows	= defaultCallbackThrows;
function defaultCallbackThrows(err){
	if(err != null){
		throw err;
	}
}
