// vim: ft=javascript noet ts=4 sts=4 sw=4

const util					= require("util");

//==============================================================================

module.exports.num2str		= num2str;
function num2str(num, digit, ratio){
	let str;

	if(!Number.isFinite(num)){
		let srcRatio;
		let isFloat		= false;
		if(/^\s*\-?\s*0[0-7]+\s*$/.test(num)){
			srcRatio		= 8;
		} else
		if(/^\s*\-?\s*\d+(\.\d+)?\s*$/.test(num)){
			srcRatio		= 10;
			isFloat			= RegExp.$1 != "";
		} else
		if(/^\s*\-?\s*(0[xX]\s*[\da-fA-F]+|[\da-fA-F]*[a-fA-F][\da-fA-F]*)\s*$/.test(num)){
			srcRatio		= 16;
		}

		//console.log(num, srcRatio, ratio, isFloat);

		if(srcRatio == ratio){
			str		= new String(num);
		} else {
			num		= (isFloat ? Number.parseFloat : Number.parseInt)(num, srcRatio);
			str		= new Number(num).toString(ratio);
		}
	} else {
		str		= new Number(num).toString(ratio);
	}

	let sign, intPart, decPart;
	if(/^\s*([\-\+]\s*)?(0[xX]\s*)?([\da-fA-F]+)(\.\d+)?\s*$/.test(str)){
		sign		= RegExp.$1;
		head		= RegExp.$2;
		intPart		= RegExp.$3;
		decPart		= RegExp.$4;
	} else {
		return str;
	}

	if(digit==null || !Number.isFinite(digit) || digit<=0){
		//Thousand separator mode
		let bottomBase	= false;

		let sep;
		let sepWidth;
		switch(ratio){
		case 16:	sep = " ";		sepWidth = 4;	bottomBase = false;		break;
		case 10:	sep = ",";		sepWidth = 3;	bottomBase = true;		break;
		case  8:	sep = " ";		sepWidth = 3;	bottomBase = true;		break;
		default:
			if(ratio == null){
				sep			= ",";
				sepWidth	= 3;
				bottomBase	= true;
			}
			break;
		}

		if(sep != null){
			if(head!="" && sep==" " && intPart.length>sepWidth){
				head	= head + sep;
			}

			intPart		= intPart.replace(
							bottomBase
								? new RegExp(`([\\da-fA-F])(?=(?:[\\da-fA-F]{${sepWidth}})+(?:$|[^\\da-fA-F]))`, "g")
								: new RegExp(`([\\da-fA-F]{${sepWidth}})(?=[\\da-fA-F])`, "g")
							,	`$1${sep}`
						);

			str			= `${sign}${head}${intPart}${decPart}`;

			//console.log("%s : %s : %s : %s => %s", sign, head, intPart, decPart, str);
		}
	} else {
		//0-filling mode
		str			= util.format("%s%s%s%s%s",
						sign,
						head,
						"0".repeat(Math.max(digit - sign.length - head.length - intPart.length - decPart.length, 0)),
						intPart,
						decPart
					);
	}

	return str;
}
