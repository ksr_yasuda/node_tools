// vim: ft=javascript noet ts=4 sts=4 sw=4

const util					= require("util");

const num2str				= require("./number.js").num2str;

//==============================================================================

module.exports.dateFormat		= dateFormat;
function dateFormat(date, format){
	function strEscape(str){
		return str.replace(/(\w)/gm, "\\$1");
	}

	const strDayL	= ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
	const strDayS	= strDayL.map(function (s){return s.substring(0, 3);});
	const strMonthL	= ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
	const strMonthS	= strMonthL.map(function (s){return s.substring(0, 3);});
	const strAMPML	= ["AM", "PM"];
	const strAMPMS	= strAMPML.map(function (s){return s.substring(0, 1);});

	function getTimeZoneOffset(d){
		let timeZoneOffset	= d.getTimezoneOffset();

		return	[
					timeZoneOffset <= 0,
					Math.floor(Math.abs(timeZoneOffset) / 60),
					           Math.abs(timeZoneOffset) % 60
				];
	}

	const dateSpec	= {
		yyyy:	function (d){return num2str(d.getFullYear(),     4);},
		yyy:	function (d){return num2str(d.getFullYear(),     3);},
		yy:		function (d){return num2str(d.getFullYear()%100, 2);},
		y:		function (d){return         d.getFullYear()%100;},

		MMMM:	function (d){return strMonthL[d.getMonth()];},
		MMM:	function (d){return strMonthS[d.getMonth()];},
		MM:		function (d){return num2str(d.getMonth()+1, 2);},
		M:		function (d){return         d.getMonth()+1;},

		dddd:	function (d){return strDayL[d.getDay()];},
		ddd:	function (d){return strDayS[d.getDay()];},
		dd:		function (d){return num2str(d.getDate(), 2);},
		d:		function (d){return         d.getDate();},

		tt:		function (d){return d.getHours()<12 ? strAMPML[0] : strAMPML[1];},
		t:		function (d){return d.getHours()<12 ? strAMPMS[0] : strAMPMS[1];},

		hh:		function (d){return num2str(d.getHours()%12==0 ? 12 : d.getHours()%12, 2);},
		h:		function (d){return         d.getHours()%12==0 ? 12 : d.getHours()%12;},
		HH:		function (d){return num2str(d.getHours(), 2);},
		H:		function (d){return         d.getHours();},

		mm:		function (d){return num2str(d.getMinutes(), 2);},
		m:		function (d){return         d.getMinutes();},

		ss:		function (d){return num2str(d.getSeconds(), 2);},
		s:		function (d){return         d.getSeconds();},

		fff:	function (d){return num2str(           d.getMilliseconds(),      3);},
		ff:		function (d){return num2str(Math.floor(d.getMilliseconds()/ 10), 2);},
		f:		function (d){return         Math.floor(d.getMilliseconds()/100);},

		K:		function (d){
					let tz	= getTimeZoneOffset(d);
					if(tz[1]==0 && tz[2]==0){
						return "Z";
					} else {
						return	(tz[0] ? "+" : "-")		+
								num2str(tz[1], 2)		+ ":" +
								num2str(tz[2], 2);
					}
				},
		zzz:	function (d){
					let tz	= getTimeZoneOffset(d);
					return	(tz[0] ? "+" : "-")		+
							num2str(tz[1], 2)		+ ":" +
							num2str(tz[2], 2);
				},
		zz:		function (d){
					let tz	= getTimeZoneOffset(d);
					return	(tz[0] ? "+" : "-")		+
							num2str(tz[1], 2);
				},
		z:		function (d){
					let tz	= getTimeZoneOffset(d);
					return	(tz[0] ? "+" : "-")		+
							tz[1];
				}
	};

	//----------------------------------------------------------

	if(date == null){
		return format;
	}
	if(format == null){
		return d;
	}

	//----------------------------------------------------------

	format		= [{str:	format}];

	function updateFormat(exp, replaceFunc){
		let newFormat		= [];

		for(let i=0; i<format.length; i++){
			if(format[i].replaced){
				newFormat.push(format[i]);
				continue;
			}

			let target	= format[i].str;
			while(true){
				if(exp.test(target)){
					let leftContext		= RegExp.leftContext;
					let rightContext	= RegExp.rightContext;

					newFormat.push({
						str:		leftContext
					});
					newFormat.push({
						str:		replaceFunc(),
						replaced:	true
					});
					target	= rightContext;
				} else {
					newFormat.push({
						str:		target
					});
					break;
				}
			}
		}
		format		= newFormat;
	}

	//..........................................................

	updateFormat(/\\(.)|(['"])(.*?)\2/m,
		function(){
			return RegExp.$1 + RegExp.$3;
		}
	);

	for(let spec in dateSpec){
		updateFormat(new RegExp(spec, "m"),
			function(){
				return dateSpec[spec](date);
			}
		);
	}

	//----------------------------------------------------------

	return format.map(function (v){return v.str;}).join("");
}

Date.prototype.toStringOrg				= Date.prototype.toString;
Date.prototype.toString					=
	function (format){
		if(format == null){
			return this.toStringOrg();
		} else {
			return dateFormat(this, format);
		}
	};

//------------------------------------------------------------------------------

module.exports.date2localISO8601Str		= date2localISO8601Str;
function date2localISO8601Str(date){
	if(date == null){
		return null;
	}

	return dateFormat(date, "yyyy-MM-dd'T'HH:mm:ss.fffK");
}

Date.prototype.toUTCISOString			= Date.prototype.toISOString;
Date.prototype.toISOString				=
Date.prototype.toLocaleISOString		=
	function (){
		return date2localISO8601Str(this);
	};
