// vim: ft=javascript noet ts=4 sts=4 sw=4

module.exports.UTF8_BOM		= Buffer.from("EFBBBF", "hex");
