// vim: ft=javascript noet ts=4 sts=4 sw=4

Object.deepFreeze			= deepFreeze;
function deepFreeze(obj){
	Object.freeze(obj);

	for(let key in obj){
		let val		= obj[key];

		if(!obj.hasOwnProperty(key)
		|| !(typeof val === "object")
		|| Object.isFrozen(val))
		{
			continue;
		}

		deepFreeze(val);
	}

	return obj;
}
