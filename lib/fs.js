// vim: ft=javascript noet ts=4 sts=4 sw=4

const fs					= require("fs");
const path					= require("path");
const util					= require("util");
const os					= require("os");
const assert				= require("assert");

const globWin				= require("./glob.js").glob;
const globWinSync			= require("./glob.js").globSync;
const dateFormat			= require("./date.js").dateFormat;

//==============================================================================

module.exports.mkdirRecursive	=
module.exports.pMkdirRecursive	= pMkdirRecursive;
function pMkdirRecursive(targetPath, callback){
	if(targetPath==null || targetPath==""){
		return;
	}

	//----------------------------------------------------------

	let isAbsolutePath		= path.isAbsolute(targetPath);
	targetPath				= targetPath.split(/[\/\\]/);
	if(os.type()!="Windows_NT" && isAbsolutePath){
		assert.equal(targetPath[0], "");
		targetPath[0] = path.sep;
	}

	//console.log(targetPath);
	return (async () => {
		for(let i=1; i<=targetPath.length; i++){
			let t	= path.join.apply(path.join, targetPath.slice(0, i));
			if(!fs.existsSync(t)){
				await util.promisify(fs.mkdir)(t);
			}
		}
	})().then(
		callback==null ? null :
			function (){
				callback();
			},
		callback==null ? null :
			function (err){
				callback(err);
			}
	);
}

//==============================================================================

module.exports.rmRecursiveSync		= rmRecursiveSync;
function rmRecursiveSync(targetPath){
	if(targetPath==null || targetPath==""){
		return;
	}

	//----------------------------------------------------------

	targetPath = globWinSync(targetPath, {dot: true});

	targetPath.forEach(function (target, index, arr){
		//console.log("Remove : %s", target);
		if(fs.existsSync(target)){
			let stats		= fs.statSync(target);
			if(stats.isDirectory()){
				rmRecursiveSync(path.join(target, "*"));
				fs.rmdirSync(target);
			} else {
				fs.unlinkSync(target);
			}
			//console.log("Removed: %s", target);
		}
	});
}

//------------------------------------------------------------------------------

module.exports.rmRecursive		=
module.exports.pRmRecursive		= pRmRecursive;
function pRmRecursive(targetPath, callback){
	if(targetPath==null || targetPath==""){
		return;
	}

	//----------------------------------------------------------

	return (async () => {
		targetPath = await	globWin(targetPath, {dot: true});

		await	Promise.all(targetPath.map(function (target, index, arr){
					return	(async () => {
								//console.log("Remove : %s", target);
								if(fs.existsSync(target)){
									let stats		= await util.promisify(fs.stat)(target);
									if(stats.isDirectory()){
										await pRmRecursive(path.join(target, "*"));
										await util.promisify(fs.rmdir)(target);
									} else {
										await util.promisify(fs.unlink)(target);
									}
									//console.log("Removed: %s", target);
								}
							})();
				}));
	})().then(
		callback==null ? null :
			function (){
				callback();
			},
		callback==null ? null :
			function (err){
				callback(err);
			}
	);
}

//==============================================================================

module.exports.cpRecursive		=
module.exports.pCpRecursive		= pCpRecursive;
function pCpRecursive(src, dst, callback){
	//console.log("%s -> %s", src, dst);

	if(!fs.existsSync(src)){
		return;
	}

	//----------------------------------------------------------

	return (async () => {
		let stats	= await util.promisify(fs.stat)(src);

		if(stats.isDirectory()){
			await util.promisify(fs.mkdir)(dst, stats.mode);

			let files	= await util.promisify(fs.readdir)(src);

			await	Promise.all(files.map(function (element, index, self){
						return	pCpRecursive(
									path.join(src, element),
									path.join(dst, element)
								);
					}));
		} else {
			await new Promise((resolve, reject) => {
				let srcStream	= fs.createReadStream(src, {autoClose: true});
				let dstStream	= fs.createWriteStream(dst, {
									autoClose:		true
								,	mode:			stats.mode
								});
				dstStream.on("close", () => {
					//console.log("Close %s", dst);
					resolve();
				});
				srcStream.pipe(dstStream);
			});
		}
	})().then(
		callback==null ? null :
			function (){
				callback();
			},
		callback==null ? null :
			function (err){
				callback(err);
			}
	);
}

//==============================================================================

module.exports.tmpFileName		= tmpFileName;
function tmpFileName(fileName){
	let dateStrFormat;
	let date;

	for(let i=1; i<arguments.length; i++){
		if(arguments[i] == null){
			continue;
		}

		switch(typeof arguments[i]){
		case "string":
			dateStrFormat		= arguments[i];
			break;

		case "object":
			if(arguments[i] instanceof Date){
				date			= arguments[i];
			} else {
				dateStrFormat	= arguments[i].toString();
			}
			break;

		default:
			break;
		}
	}

	let separator	= "_";
	let basename, extname;
	if(fileName == null){
		baseName	=
		extName		= "";
	} else {
		baseName	= path.basename(fileName);
		let dirName	= fileName.substring(0, fileName.length - baseName.length);
		baseName	= baseName.split(".");
		extName		= [];
		if(baseName[0]=="" && baseName.length>=2){
			//Dot starting files
			baseName.splice(0, 2, `.${baseName[1]}`);
		}

		if(baseName.length >= 2){
			extName.push(baseName.pop());
		}

		if(baseName.length>=2 && extName.length>=1){
			//.gz, .bz2, .xz, .zst, .lz4, .bak, .org
			assert.equal(extName.length, 1
			,	`Unexpectedly file extension has too much length: ${extName}`
			);
			switch(extName[0].toLowerCase()){
			case "gz":
			case "bz2":
			case "xz":
			case "zst":
			case "lz4":
			case "bak":
			case "org":
				extName.unshift(baseName.pop());
				break;

			default:
				switch (baseName[baseName.length - 1].toLowerCase()) {
				case "tar":
					extName.unshift(baseName.pop());
					break;

				default:
					break;
				}
				break;
			}
		}

		if(baseName.length >= 2){
			//Have dot in basename
			separator		= ".";
		}
		else {
			let hit		= [];
			for (let h of
				baseName.map(n =>
					Array.from(
						n.toLowerCase().matchAll('(?<=^|[_.-])([0-9x]{4})[_.-]?([0-9x]{2})(?:[_.-]?([0-9x]{2}|[bmex]))?(?=[_.-]|$)')
					)
				)
			) {
				hit.push(...h);
			}
			//console.log(hit);
			for (let s of hit) {
				let y		= s[1] || "";
				let m		= s[2] || "";
				let d		= s[3] || "";
				let date	= `${y}${m}${d}`;
				//console.log(date);

				if (/x[^x]/.test(date)) {
					continue;
				}

				if (/(19|20|21|xx)[0-9x]{2}(0[1-9x]|1[0-2x]|xx)([0-2x][0-9x]|3[0-1x]|xx|[bmex]|)/.test(date)){
					separator		= ".";
					break;
				}
			}
		}

		baseName	= dirName + baseName.join(".");
		extName		= extName.length==0 ? "" : `.${extName.join(".")}`;
	}

	if(dateStrFormat == null){
		dateStrFormat	= "yyyyMMdd_HHmmss_fff";
	}

	if(date == null){
		date = new Date();
	}

	//----------------------------------------------------------

	return	util.format("%s%s%s%s",
				baseName,
				baseName=="" ? "" : separator,
				dateFormat(date, dateStrFormat),
				extName
			);
}
