// vim: ft=javascript noet ts=4 sts=4 sw=4

const fs					= require("fs");
const path					= require("path");
const os					= require("os");
const childProcess			= require("child_process");
const util					= require("util");
const assert				= require("assert");

const glob					= require("glob");

//==============================================================================

function pathConvSeparator(pathStr, oldSep, newSep){
	return pathStr.split(oldSep).join(newSep);
}

//------------------------------------------------------------------------------

function pathConvSeparatorWin2Posix(pathStr){
	return pathConvSeparator(pathStr, path.win32.sep, path.posix.sep);
}

//------------------------------------------------------------------------------

function pathConvSeparatorPosix2Win(pathStr){
	return pathConvSeparator(pathStr, path.posix.sep, path.win32.sep);
}

//==============================================================================

function findEmptyDriveLetter(){
	const driveLetters	= "ABCDEFGHIJKLMNOPQRSTUVWXYZ".split("")
							.map(function (drive){
								return drive + ":"
							}).reverse();

	for(drive of driveLetters){
		if(!fs.existsSync(drive)){
			return drive;
		}
	}
}

//------------------------------------------------------------------------------

function replaceRoot(pathStr, oldRoot, newRoot){
	let pathParse	= path.parse(pathStr);
	let pathRoot	= pathParse.root;
	let pathBase	= path.relative(pathRoot, pathStr);
	//console.log(pathBase);
	//console.log(pathRoot);
	//console.log(oldRoot);
	//console.log(newRoot);

	if(pathRoot==oldRoot || pathRoot+path.sep==oldRoot || pathRoot==oldRoot+path.sep){
		return path.join(newRoot, pathBase);
		//console.log(pathStr);
	} else {
		return pathStr;
	}
}

//------------------------------------------------------------------------------

let mountDriveList		= [];
function getMountDriveEntry(targetPath){
	let mountDriveEntry		= mountDriveList.filter(function (entry){
								return entry.targetPath == targetPath
							});

	if(mountDriveEntry.length == 0){
		let mountDrive = findEmptyDriveLetter();
		mountDriveEntry		= {
								driveLetter		: mountDrive,
								targetPath		: targetPath,
								refCount		: 0
							};
		mountDriveList.push(mountDriveEntry);

		childProcess.execSync(
			`subst "${mountDrive}" "${targetPath}"`
		);
	} else {
		assert.equal(mountDriveEntry.length, 1);

		mountDriveEntry		= mountDriveEntry[0];
		assert.equal(mountDriveEntry.targetPath, targetPath);
	}
	assert.notEqual(mountDriveEntry, null);
	mountDriveEntry.refCount++;
	//console.log(mountDriveEntry);

	return mountDriveEntry;
}

//------------------------------------------------------------------------------

function releaseMountDrive(mountDrive){
	if(mountDrive==null || mountDrive==""){
		//console.log("Skip drive release");
		return;
	}
	//console.log("Drive release: %s", mountDrive);

	let entryIndex;
	let mountDriveEntry;
	for(entryIndex=0; entryIndex<mountDriveList.length; entryIndex++){
		mountDriveEntry		= mountDriveList[entryIndex];
		if(mountDriveEntry.driveLetter == mountDrive){
			break;
		}
	}
	assert(entryIndex < mountDriveList.length, "Entry index");
	assert(mountDriveEntry.refCount > 0, "Entry refCount");

	mountDriveEntry.refCount--;
	//console.log(mountDriveEntry);

	if(mountDriveEntry.refCount == 0){
		assert.ok(fs.existsSync(mountDriveEntry.driveLetter));

		childProcess.execSync(
			`subst /D "${mountDriveEntry.driveLetter}"`
		);

		mountDriveList.splice(entryIndex, 1);
	}
}

//------------------------------------------------------------------------------

function pathConvNetwork2Mount(pathStr){
	//console.log(pathStr);

	let mountDrive		= "";
	let pathRoot		= "";
	if(/^\\\\/.test(pathStr)){
		//Network dir
		let pathParse	= path.parse(pathStr);
		pathRoot		= pathParse.root;

		let pathBase	= path.relative(pathRoot, pathStr);
		//console.log(pathRoot);
		//console.log(pathBase);

		let mountDriveEntry		= getMountDriveEntry(pathRoot);
		mountDrive				= mountDriveEntry.driveLetter;
		//console.log(mountDriveEntry);

		pathStr		= replaceRoot(pathStr, pathRoot, mountDrive);
	}

	return [pathStr, pathRoot, mountDrive];
}

//..............................................................................

function pathConvMount2Network(pathStr, networkPath, mountDrive){
	return replaceRoot(pathStr, mountDrive, networkPath);
}

//==============================================================================

module.exports.glob		= os.type()=="Windows_NT" ? globWin : glob.glob;
function globWin(pattern){
	let args				= Array.prototype.concat.apply([], arguments);
	let callback;

	if(args.length >= 2){
		let callbackIndex	= args.length - 1;
		callback			= args[callbackIndex];
		args.splice(callbackIndex, 1);

		if(typeof callback != "function"){
			callbackIndex	=
			callback		= null;
		}
	}

	//..........................................................

	let networkPath, mountDrive;

	return (async () => {
		try{
			//XXX:	glob only handles posix directory separator, '/'
			[pattern, networkPath, mountDrive] = pathConvNetwork2Mount(pattern);
			assert.notEqual(mountDrive, null);

			args[0]				=
			pattern				= pathConvSeparatorWin2Posix(pattern);

			let result		= await glob.glob(...args);
			result			= result.map(pathConvSeparatorPosix2Win)
								.map(function (pathStr){
									return pathConvMount2Network(pathStr, networkPath, mountDrive);
								});

			return result;
		} finally{
			releaseMountDrive(mountDrive);
		}
	})().then(
		callback==null ? null :
			function (result){
				callback(null, result);
			},
		callback==null ? null : callback
	);
}

//------------------------------------------------------------------------------

module.exports.glob.sync	= os.type()=="Windows_NT" ? globWinSync : glob.globSync;
function globWinSync(pattern){
	let args		= Array.prototype.concat.apply([], arguments);
	let result		= [];

	let networkPath, mountDrive;
	try{
		[pattern, networkPath, mountDrive] = pathConvNetwork2Mount(pattern);

		//XXX:	glob only handles posix directory separator, '/'
		args[0]			=
		pattern			= pathConvSeparatorWin2Posix(pattern);

		result			= glob.globSync(...args)
							.map(pathConvSeparatorPosix2Win)
							.map(function (pathStr){
								return pathConvMount2Network(pathStr, networkPath, mountDrive);
							});
	} finally{
		releaseMountDrive(mountDrive);
	}

	return result;
}
