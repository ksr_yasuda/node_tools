// vim: ft=javascript noet ts=4 sts=4 sw=4

const assert				= require("assert");
const util					= require("util");

//==============================================================================

module.exports.p			= util.deprecate(p, "Use util.promisify() instead.");
function p(f, args, thisObj){
	if(args == null){
		args = [];
	}

	return	new Promise(function (resolve, reject){
				if(f == null){
					reject(new Error("Given function is not defined"));
					return;
				}

				if(typeof f !== "function"){
					reject(new Error("Given function is bad"));
					return;
				}

				assert.notEqual(args, null);
				if(typeof args!=="object" || !Number.isInteger(args.length)){
					reject(new Error("Given arguments is bad"));
					return;
				}

				f.apply(thisObj,
					args.concat(function (err, val){
						if(err == null){
							if(arguments.length > 2){
								val		= Array.prototype.concat.apply([], arguments);
								val.shift();
							}

							resolve(val);
						} else {
							reject(err);
						}
					})
				);
			});
}
