# What is this?

NodeJS tool library

# How to install

```
npm install --save git+http://bitbucket.org/ksr_yasuda/node_tools.git
```

# APIs

## Constants

### `UTF8_BOM`

UTF-8 BOM (`0xEFBBBF`)

## Promise

### `p(f, [args], [thisObj])`

Exec given function to return a Promise object.

Use `util.promisify()` instead on Node.js v8.0.0 or later.

|Parameter      |Description                                                    |
|:--------------|:--------------------------------------------------------------|
|`f`            |Function to call                                               |
|`[args]`       |An array of arguments to give the function                     |
|`[thisObj]`    |`this` object of the function execution (default: global env)  |

## Callback

### `defaultCallback(err)`

Callback function to show detail of given error object.

|Parameter      |Description                                                    |
|:--------------|:--------------------------------------------------------------|
|`err`          |Error object                                                   |

### `defaultCallbackThrows(err)`

Callback function just throw given error object.

|Parameter      |Description                                                    |
|:--------------|:--------------------------------------------------------------|
|`err`          |Error object                                                   |

## Glob

Wrap functions of [glob][npm_glob] library for windows.

On other OS, just call the official [glob][npm_glob].

[npm_glob]: https://www.npmjs.com/package/glob

### `glob(pattern, [options], [cb])`

Search entries for glob pattern.

|Parameter      |Description                                                    |
|:--------------|:--------------------------------------------------------------|
|`pattern`      |Glob pattern to search                                         |
|`[options]`    |Options                                                        |
|`[cb]`         |Callback function                                              |

### `glob.sync(pattern, [options])`

Synchronous version of `glob(pattern, [options], [cb])`

|Parameter      |Description                                                    |
|:--------------|:--------------------------------------------------------------|
|`pattern`      |Glob pattern to search                                         |
|`[options]`    |Options                                                        |

## File System

### `mkdirRecursive(targetPath, [callback])`

Recursively create directory like `mkdir -p` command.

This returns a `Promise` object.

|Parameter      |Description                                                    |
|:--------------|:--------------------------------------------------------------|
|`targetPath`   |Create directory path                                          |
|`[callback]`   |Callback function (To call as `Promise`, skip it)              |

### `rmRecursive(targetPath, [callback])`

Recursively remove entries like `rm -r` command.

This returns a `Promise` object.

|Parameter      |Description                                                    |
|:--------------|:--------------------------------------------------------------|
|`targetPath`   |Remove entry path or glob pattern                              |
|`[callback]`   |Callback function (To call as `Promise`, skip it)              |

### `rmRecursiveSync(targetPath)`

Synchronous version of `rmRecursive(targetPath, callback)`.

|Parameter      |Description                                                    |
|:--------------|:--------------------------------------------------------------|
|`targetPath`   |Remove entry path or glob pattern                              |

### `cpRecursive(src, dst, [callback])`

Recursively copy entries like `cp -r` command.

This returns a `Promise` object.

|Parameter      |Description                                                    |
|:--------------|:--------------------------------------------------------------|
|`targetPath`   |Copy entry path                                                |
|`[callback]`   |Callback function (To call as `Promise`, skip it)              |

### `tmpFileName(fileName, [format], [date])`

Create temp file name string with current timestamp.

This dirties RegExp object.

|Parameter      |Description                                                    |
|:--------------|:--------------------------------------------------------------|
|`fileName`     |Base filename                                                  |
|`[format]`     |Date format (See `dateFormat(date, format)`)                   |
|`[date]`       |Date to use in the filename (Default is the current time)      |

## Number

### `num2str(num, [digit], [ratio])`

Convert given number to a string with given digit filling with `0`
(**e.g.** `12` -> `00012`).

Or giving `digit` non-number, 0 or less, work to add thousand separator
(**e.g.** `12345` -> `12,345`).

This dirties RegExp object.

|Parameter      |Description                                                            |
|:--------------|:----------------------------------------------------------------------|
|`num`          |Number (or string) to convert                                          |
|`[digit]`      |Width of digit (If non-number or 0 or less, run thousand sep mode)     |
|`[ratio]`      |Number ratio converted to (Default: 10)                                |

## Date

### `dateFormat(date, format)`

Convert given date object to a string in given format.

This dirties RegExp object.

|Parameter      |Description                                                    |
|:--------------|:--------------------------------------------------------------|
|`date`         |Date to convert                                                |
|`format`       |Date format                                                    |

Time format is specified as below:

|Format String  |Description                                                                |
|:--------------|:--------------------------------------------------------------------------|
|`yyyy`         |Year in at least 4 digit width (**e.g.** `0602`)                           |
|`yyy`          |Year in at least 3 digit width (**e.g.** `602`)                            |
|`yy`           |Year in 2 digit width (**e.g.** `02`)                                      |
|`y`            |Year in 1 digit width (**e.g.** `2`)                                       |
|`MMMM`         |Month in long month name (**e.g.** `January`)                              |
|`MMM`          |Month in short month name (**e.g.** `Jan`)                                 |
|`MM`           |Month in 2 digit width (**e.g.** `01`)                                     |
|`M`            |Month in 1 digit width (**e.g.** `1`)                                      |
|`dddd`         |Day of week in long day name (**e.g.** `Monday`)                           |
|`ddd`          |Day of week in short day name (**e.g.** `Mon`)                             |
|`dd`           |Day of the month in 2 digit width (**e.g.** `01`)                          |
|`d`            |Day of the month in 1 digit width (**e.g.** `1`)                           |
|`tt`           |AM/PM in long format (**e.g.** `AM`)                                       |
|`t`            |AM/PM in short format (**e.g.** `A`)                                       |
|`hh`           |Hours in 12-hour clock in 2 digit width (**e.g.** `01`)                    |
|`h`            |Hours in 12-hour clock in 1 digit width (**e.g.** `1`)                     |
|`HH`           |Hours in 24-hour clock in 2 digit width (**e.g.** `01`, `13`)              |
|`H`            |Hours in 24-hour clock in 1 digit width (**e.g.** `1`, `13`)               |
|`mm`           |Minutes in 2 digit width (**e.g.** `01`)                                   |
|`m`            |Minutes in 1 digit width (**e.g.** `1`)                                    |
|`ss`           |Seconds in 2 digit width (**e.g.** `01`)                                   |
|`s`            |Seconds in 1 digit width (**e.g.** `1`)                                    |
|`fff`          |Milliseconds in 3 digit width (**e.g.** `001`)                             |
|`ff`           |Milliseconds in 2 digit width (**e.g.** `01`)                              |
|`f`            |Milliseconds in 1 digit width (**e.g.** `1`)                               |
|`K`            |Time zone string (**e.g.** `Z` for UTC, or the same of `zzz` for others)   |
|`zzz`          |Time zone string in long format (**e.g.** `+09:00`)                        |
|`zz`           |Time zone string in 2 digit width (**e.g.** `+09`)                         |
|`z`            |Time zone string in 1 digit width (**e.g.** `+9`)                          |

### `date2localISO8601Str(date)`

Convert given date object to ISO-8601 date format of local timezone (`yyyy-MM-dd'T'HH:mm:ss.fffK`).

The methods of `Data` object below are also set to this function.
So `JSON.stringify()`-ed `Date` object is also converted in this format.

* [`Date.prototype.toISOString()`][Date_prototype_toISOString]
* `Date.prototype.toLocaleISOString()`

The original `Date.prototype.toISOString()` is saved as `Date.prototype.toUTCISOString()`.

[Date_prototype_toISOString]: https://developer.mozilla.org/ja/docs/Web/JavaScript/Reference/Global_Objects/Date/toISOString

This dirties RegExp object.

|Parameter      |Description                                                    |
|:--------------|:--------------------------------------------------------------|
|`date`         |Date to convert                                                |

## Object

### `Object.deepFreeze(obj)`

Call [`Object.freeze(obj)`][Object_freeze] recursively.

See [`Object.freeze(obj)`][Object_freeze].

[Object_freeze]: https://developer.mozilla.org/ja/docs/Web/JavaScript/Reference/Global_Objects/Object/freeze

|Parameter      |Description                                                    |
|:--------------|:--------------------------------------------------------------|
|`obj`          |Object to freeze                                               |

# License
MIT
