Change Log
================================================================================

v2.6.4 - 2024-05-29
--------------------------------------------------------------------------------

### Fixed

* Remove unnecessary `util.prpmisify()` in test
* Update submodule
    - glob : v10.2.7 -> v10.4.1

v2.6.3 - 2023-06-09
--------------------------------------------------------------------------------

### Fixed

* `tmpFileName()`: Extension handling: Consider lower / upper cases

### Changed

* `tmpFileName()`: Extension handling: Support `tar.XXX`

v2.6.2 - 2023-06-07
--------------------------------------------------------------------------------

### Fixed

* `rmRecursive()`: Fix not working on Linux env due to glob update

v2.6.1 - 2023-06-07
--------------------------------------------------------------------------------

### Changed

* `tmpFileName()`: Support extension `zst`, `lz4`

v2.6.0 - 2023-06-07
--------------------------------------------------------------------------------

### Changed

* `tmpFileName()`: Fix naming if given basename contains `x`-masked date-like string
* Update dependency
    * `glob`: v10.2.6 -> v10.2.7

v2.5.0 - 2023-06-02
--------------------------------------------------------------------------------

### Changed

* Update dependency
    * `glob`: v2.7.3 -> v10.2.6

v2.4.1 - 2023-06-02
--------------------------------------------------------------------------------

### Changed

* Update dependency
    * `glob`: v7.2.0 -> v7.2.3

v2.4.0 - 2023-06-02
--------------------------------------------------------------------------------

### Changed

* `tmpFileName()`: Fix naming if given basename contains date-like string

v2.3.2 - 2022-01-13
--------------------------------------------------------------------------------

### Fixed

* `tmpFileName()`: Fix extension handling with dot-appearance in dirname

### Added

* Added `CHANGELOG.md`

v2.3.1 - 2018-06-14
--------------------------------------------------------------------------------

### Changed

* package.json: Set target Node.js engines

v2.3.0 - 2018-06-14
--------------------------------------------------------------------------------

### Fixed

* Test: `glob()`: Fixed native-glob()'s expected return value

### Changed

* `tmpFileName()`: Improved file extension handling

v2.2.1 - 2018-02-13
--------------------------------------------------------------------------------

### Changed

* `cpRecursive()`: Changed to save file permission

v2.2.0 - 2017-12-21
--------------------------------------------------------------------------------

### Deprecated

* `p()`: Set deprecated

### Changed

* Replaced `p()` call with `util.promisify()`

v2.1.0.1 - 2017-11-09
--------------------------------------------------------------------------------

### Changed

* `README.md`: Some changes

v2.1.0 - 2017-11-08
--------------------------------------------------------------------------------

### Added

* `num2str()`: Added thousand separator mode
* `num2str()`: Added `string`-input support

### Fixed

* `dateFormat()`: Fixed formatting bug in using new `num2str()`

v2.0.1.1 - 2017-11-07
--------------------------------------------------------------------------------

### Fixed

* `README.md`: Some fixes

v2.0.1 - 2017-11-06
--------------------------------------------------------------------------------

### Added

* Added `README.md`

v2.0.0 - 2017-10-13
--------------------------------------------------------------------------------

### Added

* Added tests

### Changed

* Divided source file

<!-- vim: set ft=markdown et ts=4 sts=4 sw=4: -->
