#!/usr/bin/env node
// vim: ft=javascript noet ts=4 sts=4 sw=4

const assert		= require("assert");
const path			= require("path");
const fs			= require("fs");
const util			= require("util");

const glob			= require("glob");

const t				= require("../../index.js");

//==============================================================================

const testDir		= "TEMP";

//==============================================================================

(async () => {
	try{
		assert.notEqual(t.rmRecursive,				null);
		assert.notEqual(t.pRmRecursive,				null);

		//----------------------------------------------------------------------

		if(fs.existsSync(testDir)){
			await util.promisify(t.rmRecursive)(testDir);
		}

		//======================================================================

		assert.notEqual(t.mkdirRecursive,			null);
		assert.notEqual(t.pMkdirRecursive,			null);

		//----------------------------------------------------------------------

		await util.promisify(t.mkdirRecursive)(path.join(testDir, "hoge", "fuga"));

		await util.promisify(fs.writeFile)(path.join(testDir, "hoge", "fuga", "piyo1.txt"), "Data1");
		await util.promisify(fs.writeFile)(path.join(testDir, "hoge", "fuga", "piyo2.txt"), "Data2");

		let dirResult		= await glob.glob(path.posix.join(testDir, "hoge", "**"));
		dirResult			= dirResult.map(d => d.split(path.sep).join(path.posix.sep));
		//console.log(dirResult);

		assert(dirResult.includes("TEMP/hoge"));
		assert(dirResult.includes("TEMP/hoge/fuga"));
		assert(dirResult.includes("TEMP/hoge/fuga/piyo1.txt"));
		assert(dirResult.includes("TEMP/hoge/fuga/piyo2.txt"));
		assert.deepEqual(dirResult.length, 4);

		//======================================================================

		assert.notEqual(t.cpRecursive,				null);
		assert.notEqual(t.pCpRecursive,				null);

		await t.cpRecursive(path.join(testDir, "hoge"), path.join(testDir, "hogeCopy"));

		let dirCopyResult		= await glob.glob(path.posix.join(testDir, "hogeCopy", "**"));
		dirCopyResult			= dirCopyResult.map(d => d.split(path.sep).join(path.posix.sep));
		//console.log(dirCopyResult);

		assert(dirCopyResult.includes("TEMP/hogeCopy"));
		assert(dirCopyResult.includes("TEMP/hogeCopy/fuga"));
		assert(dirCopyResult.includes("TEMP/hogeCopy/fuga/piyo1.txt"));
		assert(dirCopyResult.includes("TEMP/hogeCopy/fuga/piyo2.txt"));
		assert.deepEqual(dirCopyResult.length, 4);
		assert.deepEqual((await glob.glob(path.posix.join(testDir, "hoge", "**"))).map(d => d.split(path.sep).join(path.posix.sep)),
		                 dirResult);

		assert.deepEqual((await util.promisify(fs.readFile)("TEMP/hogeCopy/fuga/piyo1.txt")).toString(),
		                 "Data1");
		assert.deepEqual((await util.promisify(fs.readFile)("TEMP/hogeCopy/fuga/piyo2.txt")).toString(),
		                 "Data2");

		//======================================================================

		assert.notEqual(t.tmpFileName,				null);

		//----------------------------------------------------------------------

		//console.log(t.tmpFileName("foobar.txt"));
		assert(/^foobar_\d{8}_\d{6}_\d{3}\.txt$/.test(t.tmpFileName("foobar.txt")));

		//console.log(t.tmpFileName("foobar.txt", new Date("1980-02-03 12:34:56.789")));
		assert.deepEqual(t.tmpFileName("foobar.txt"
							, new Date("1980-02-03 12:34:56.789")),
			"foobar_19800203_123456_789.txt"
		);

		//console.log(t.tmpFileName("foobar.txt"
		//					, new Date("1980-02-03 12:34:56.789")
		//					, "yyyy-MM-dd=HH-mm-ss"));
		assert.deepEqual(t.tmpFileName("foobar.txt"
							, new Date("1980-02-03 12:34:56.789")
							, "yyyy-MM-dd=HH-mm-ss"),
			"foobar_1980-02-03=12-34-56.txt"
		);

		//Dot starting files
		assert.deepEqual(t.tmpFileName(".foobar"
							, new Date("1980-02-03 12:34:56.789")),
			".foobar_19800203_123456_789"
		);
		

		assert.deepEqual(t.tmpFileName(".foobar.txt"
							, new Date("1980-02-03 12:34:56.789")),
			".foobar_19800203_123456_789.txt"
		);

		//.gz, .bz2, .xz, .zst, .lz4, .bak, .org
		for (let ext of [ "gz", "bz2", "xz", "zst", "lz4", "bak", "org" ]) {
			assert.deepEqual(t.tmpFileName(`foobar.dat.${ext}`
								, new Date("1980-02-03 12:34:56.789")),
				`foobar_19800203_123456_789.dat.${ext}`
			);

			assert.deepEqual(t.tmpFileName(`foobar.dat.${ext.toUpperCase()}`
								, new Date("1980-02-03 12:34:56.789")),
				`foobar_19800203_123456_789.dat.${ext.toUpperCase()}`
			);
		}

		//.tar
		assert.deepEqual(t.tmpFileName("foobar.tar.xxx"
							, new Date("1980-02-03 12:34:56.789")),
			"foobar_19800203_123456_789.tar.xxx"
		);

		//Have dots in basename
		assert.deepEqual(t.tmpFileName("foobar.1.txt"
							, new Date("1980-02-03 12:34:56.789")),
			"foobar.1.19800203_123456_789.txt"
		);

		assert.deepEqual(t.tmpFileName("foobar.1.txt.bak"
							, new Date("1980-02-03 12:34:56.789")),
			"foobar.1.19800203_123456_789.txt.bak"
		);

		//----------------------------------------------------------------------

		for (let sep of [ "_", "-", "." ]) {
			let presep;
			let fname;
			for (let year of [ "1980", "19xx", "19XX", "xxxx", "XXXX" ]) {
				for (let month of [ "01", "xx", "XX" ]) {
					for (let day of [ "01", "B", "b", "M", "m", "E", "e", "xx", "XX", "x", "X" ]) {
						presep		= (sep != "." && /x[^x]/.test(`${year}${month}${day}`.toLowerCase()))
										? "_" : ".";

						fname	= `foobar${sep}${year}${month}${day}${presep}19800203_123456_789.txt`;
						//console.debug(`tmpname: ${fname}`);
						assert.deepEqual(t.tmpFileName(`foobar${sep}${year}${month}${day}.txt`
											, new Date("1980-02-03 12:34:56.789")),
							fname
						);

						fname	= `foobar${sep}${year}${sep}${month}${sep}${day}${presep}19800203_123456_789.txt`
						//console.debug(`tmpname: ${fname}`);
						assert.deepEqual(t.tmpFileName(`foobar${sep}${year}${sep}${month}${sep}${day}.txt`
											, new Date("1980-02-03 12:34:56.789")),
							fname
						);
					}

					fname	= `foobar${sep}${year}${month}${presep}19800203_123456_789.txt`;
					//console.debug(`tmpname: ${fname}`);
					assert.deepEqual(t.tmpFileName(`foobar${sep}${year}${month}.txt`
										, new Date("1980-02-03 12:34:56.789")),
						fname
					);

					fname	= `foobar${sep}${year}${sep}${month}${presep}19800203_123456_789.txt`
					//console.debug(`tmpname: ${fname}`);
					assert.deepEqual(t.tmpFileName(`foobar${sep}${year}${sep}${month}.txt`
										, new Date("1980-02-03 12:34:56.789")),
						fname
					);
				}
			}
		}

		//======================================================================

		await t.rmRecursive(path.join(testDir, "*"));

		//console.log(await glob.glob([testDir]));
		assert.deepEqual(await glob.glob(testDir), [testDir]);
	} finally{
		await t.rmRecursive(testDir);
	}
})().then(() => {
	console.info("FS Test:\t OK");
}).catch(t.defaultCallback);
