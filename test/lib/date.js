#!/usr/bin/env node
// vim: ft=javascript noet ts=4 sts=4 sw=4

const assert		= require("assert");

const t				= require("../../index.js");

//==============================================================================

let date_localISO8601		= t.date2localISO8601Str(new Date());

//console.log(date_localISO8601);
assert(/^\d{4}\-\d{2}\-\d{2}T\d{2}:\d{2}:\d{2}\.\d{3}(Z|[\+\-]\d{2}:\d{2})$/.test(date_localISO8601));

//==============================================================================

console.info("Date Test:\t OK");
