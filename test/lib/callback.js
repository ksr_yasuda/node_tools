#!/usr/bin/env node
// vim: ft=javascript noet ts=4 sts=4 sw=4

const assert		= require("assert");

const t				= require("../../index.js");

//==============================================================================

assert.notEqual(t.defaultCallback,			null);
assert.notEqual(t.defaultCallbackThrows,	null);

//==============================================================================

console.info("Callback Test:\t OK");
