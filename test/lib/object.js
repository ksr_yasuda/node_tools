#!/usr/bin/env node
// vim: ft=javascript noet ts=4 sts=4 sw=4

const assert		= require("assert");

const t				= require("../../index.js");

//==============================================================================

assert.notEqual(Object.deepFreeze, null);

//==============================================================================

let obj		= {
				x:		1,
				o:		{
					y:	2
				}
			};
let x		= obj.x,
    y		= obj.o.y;

//console.log(obj);

//------------------------------------------------------------------------------

x *= 10;
y *= 10;

obj.x		= x;
obj.o.y		= y;

assert.equal	(obj.x,		x);
assert.equal	(obj.o.y,	y);

//console.log(obj);

//------------------------------------------------------------------------------

Object.deepFreeze(obj);

obj.x		*= 10;
obj.o.y		*= 10;

assert.equal	(obj.x,		x);
assert.equal	(obj.o.y,	y);

//console.log(obj);

//==============================================================================

console.info("Object Test:\t OK");
