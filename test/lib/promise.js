#!/usr/bin/env node
// vim: ft=javascript noet ts=4 sts=4 sw=4

const assert		= require("assert");

const t				= require("../../index.js");

//==============================================================================

assert.notEqual(t.p, null);
const p				= t.p;

//==============================================================================

function f(x, y, z, callback){
	callback(null, [x, y, z]);
}

//------------------------------------------------------------------------------

(async () => {
	process.noDeprecation 		= true;

	let fRet		= await p(f, [1, 2, 3]);

	process.noDeprecation 		= false;

	assert.deepEqual(fRet, [1, 2, 3]);

	//==========================================================================

})().then(() => {
	console.info("Promise Test:\t OK");
}).catch(t.defaultCallback);
