#!/usr/bin/env node
// vim: ft=javascript noet ts=4 sts=4 sw=4

const assert		= require("assert");
const util			= require("util");
const path			= require("path");

const t				= require("../../index.js");
const glob			= t.glob;

//==============================================================================

(async () => {
	const correctResult		= ["LICENSE"];
	const pattern			= "*CENSE";

	assert.deepEqual(await util.promisify(glob)(pattern),		correctResult);
	assert.deepEqual(glob.sync(pattern),						correctResult);

	//--------------------------------------------------------------------------

	const correctResultDir	= [
		"lib/callback.js"
	,	"lib/const.js"
	,	"lib/date.js"
	,	"lib/fs.js"
	,	"lib/glob.js"
	,	"lib/number.js"
	,	"lib/object.js"
	,	"lib/promise.js"
	].map(d => d.split(path.posix.sep).join(path.sep)).sort();
	const patternDir			= "lib/**/*.*";

	assert.deepEqual((await glob(patternDir)).sort(),	correctResultDir);
	assert.deepEqual(glob.sync(patternDir).sort(),						correctResultDir);


	//==========================================================================

})().then(() => {
	console.info("Glob Test:\t OK");
}).catch(t.defaultCallback);
