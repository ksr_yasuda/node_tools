#!/usr/bin/env node
// vim: ft=javascript noet ts=4 sts=4 sw=4

const assert		= require("assert");

const t				= require("../../index.js");

//==============================================================================

assert.notEqual(t.num2str, null);

//==============================================================================

assert.deepEqual(t.num2str(12, 5),						"00012");
assert.deepEqual(t.num2str(12, 1),						"12");

assert.deepEqual(t.num2str(-12, 5),						"-0012");
assert.deepEqual(t.num2str(-12, 1),						"-12");

assert.deepEqual(t.num2str(12.3, 5),					"012.3");
assert.deepEqual(t.num2str(12.3, 1),					"12.3");

assert.deepEqual(t.num2str(12, 5, 8),					"00014");
assert.deepEqual(t.num2str(12, 1, 8),					"14");

assert.deepEqual(t.num2str(12, 5, 16).toLowerCase(),	"0000c");
assert.deepEqual(t.num2str(12, 1, 16).toLowerCase(),	"c");

assert.deepEqual(t.num2str("12", 5),					"00012");
assert.deepEqual(t.num2str(new Number(12), 5),			"00012");

assert.deepEqual(t.num2str("12.3", 5),					"012.3");
assert.deepEqual(t.num2str(new Number(12.3), 5),		"012.3");

assert.deepEqual(t.num2str("-12", 5),					"-0012");
assert.deepEqual(t.num2str(new Number(-12), 5),			"-0012");

assert.deepEqual(t.num2str("-1.2", 5),					"-01.2");
assert.deepEqual(t.num2str(new Number(-1.2), 5),		"-01.2");

assert.deepEqual(t.num2str("0x12", 5),					"00018");
assert.deepEqual(t.num2str("0X12", 5),					"00018");
assert.deepEqual(t.num2str("0x1a", 5),					"00026");
assert.deepEqual(t.num2str("0X1A", 5),					"00026");
assert.deepEqual(t.num2str("012",  5),					"00010");

assert.deepEqual(t.num2str(123),						"123");
assert.deepEqual(t.num2str(123.0),						new Number(123.0).toString());
assert.deepEqual(t.num2str(123.4),						"123.4");

assert.deepEqual(t.num2str(1234),						"1,234");
assert.deepEqual(t.num2str(12345),						"12,345");
assert.deepEqual(t.num2str(123456),						"123,456");
assert.deepEqual(t.num2str(1234567),					"1,234,567");
assert.deepEqual(t.num2str(12345678),					"12,345,678");
assert.deepEqual(t.num2str(123456789),					"123,456,789");
assert.deepEqual(t.num2str(1234567890),					"1,234,567,890");

assert.deepEqual(t.num2str(1234.0001),					"1,234.0001");
assert.deepEqual(t.num2str(1234567.0001),				"1,234,567.0001");
assert.deepEqual(t.num2str(1234567890.0001),			"1,234,567,890.0001");

assert.deepEqual(t.num2str(-1234),						"-1,234");
assert.deepEqual(t.num2str(-12345),						"-12,345");
assert.deepEqual(t.num2str(-123456),					"-123,456");
assert.deepEqual(t.num2str(-1234567),					"-1,234,567");
assert.deepEqual(t.num2str(-12345678),					"-12,345,678");
assert.deepEqual(t.num2str(-123456789),					"-123,456,789");
assert.deepEqual(t.num2str(-1234567890),				"-1,234,567,890");

assert.deepEqual(t.num2str(-1234.0001),					"-1,234.0001");
assert.deepEqual(t.num2str(-1234567.0001),				"-1,234,567.0001");
assert.deepEqual(t.num2str(-1234567890.0001),			"-1,234,567,890.0001");

assert.deepEqual(t.num2str("0xabcde",				0, 16),		"0x abcd e");
assert.deepEqual(t.num2str("0x1234567890abcdef",	0, 16),		"0x 1234 5678 90ab cdef");
assert.deepEqual(t.num2str("abcde",					0, 16),		"abcd e");
assert.deepEqual(t.num2str("1234567890abcdef",		0, 16),		"1234 5678 90ab cdef");

assert.deepEqual(t.num2str("012345",				0, 8),		"012 345");
assert.deepEqual(t.num2str("0123456",				0, 8),		"0 123 456");
assert.deepEqual(t.num2str("01234567",				0, 8),		"01 234 567");

//==============================================================================

console.info("Number Test:\t OK");
